EESchema Schematic File Version 4
LIBS:ECT_1-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x06_Top_Bottom J1
U 1 1 5D94B6B4
P 1800 1650
F 0 "J1" H 1850 2067 50  0000 C CNN
F 1 "PMOD_Male" H 1850 1976 50  0000 C CNN
F 2 "" H 1800 1650 50  0001 C CNN
F 3 "~" H 1800 1650 50  0001 C CNN
	1    1800 1650
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5D94E397
P 2100 2000
F 0 "#PWR09" H 2100 1750 50  0001 C CNN
F 1 "GND" H 2105 1827 50  0000 C CNN
F 2 "" H 2100 2000 50  0001 C CNN
F 3 "" H 2100 2000 50  0001 C CNN
	1    2100 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 2000 2100 1900
Wire Wire Line
	2100 1850 2000 1850
Wire Wire Line
	2000 1950 2200 1950
Wire Wire Line
	2200 1950 2200 1850
$Comp
L Comparator:LM339 U1
U 1 1 5D9553D6
P 6100 2200
F 0 "U1" H 6300 2400 50  0000 C CNN
F 1 "LM339" H 6300 2300 50  0000 C CNN
F 2 "" H 6050 2300 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm339.pdf" H 6150 2400 50  0001 C CNN
	1    6100 2200
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM339 U1
U 2 1 5D95825B
P 6100 2650
F 0 "U1" H 6100 3017 50  0000 C CNN
F 1 "LM339" H 6100 2926 50  0000 C CNN
F 2 "" H 6050 2750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm339.pdf" H 6150 2850 50  0001 C CNN
	2    6100 2650
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM339 U1
U 3 1 5D959487
P 6100 3100
F 0 "U1" H 6100 3467 50  0000 C CNN
F 1 "LM339" H 6100 3376 50  0000 C CNN
F 2 "" H 6050 3200 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm339.pdf" H 6150 3300 50  0001 C CNN
	3    6100 3100
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM339 U1
U 4 1 5D95A2AB
P 6100 3550
F 0 "U1" H 6100 3917 50  0000 C CNN
F 1 "LM339" H 6100 3826 50  0000 C CNN
F 2 "" H 6050 3650 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm339.pdf" H 6150 3750 50  0001 C CNN
	4    6100 3550
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM339 U2
U 5 1 5D95B285
P 1650 6450
F 0 "U2" H 1608 6496 50  0000 L CNN
F 1 "LM339" H 1608 6405 50  0000 L CNN
F 2 "" H 1600 6550 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm339.pdf" H 1700 6650 50  0001 C CNN
	5    1650 6450
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM339 U2
U 1 1 5D95C85D
P 6100 4000
F 0 "U2" H 6100 4367 50  0000 C CNN
F 1 "LM339" H 6100 4276 50  0000 C CNN
F 2 "" H 6050 4100 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm339.pdf" H 6150 4200 50  0001 C CNN
	1    6100 4000
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM339 U2
U 2 1 5D95DADF
P 6100 4450
F 0 "U2" H 6100 4817 50  0000 C CNN
F 1 "LM339" H 6100 4726 50  0000 C CNN
F 2 "" H 6050 4550 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm339.pdf" H 6150 4650 50  0001 C CNN
	2    6100 4450
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM339 U2
U 3 1 5D95E809
P 6100 4900
F 0 "U2" H 6100 5267 50  0000 C CNN
F 1 "LM339" H 6100 5176 50  0000 C CNN
F 2 "" H 6050 5000 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm339.pdf" H 6150 5100 50  0001 C CNN
	3    6100 4900
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM339 U2
U 4 1 5D95F9D9
P 6100 5350
F 0 "U2" H 6100 5717 50  0000 C CNN
F 1 "LM339" H 6100 5626 50  0000 C CNN
F 2 "" H 6050 5450 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm339.pdf" H 6150 5550 50  0001 C CNN
	4    6100 5350
	1    0    0    -1  
$EndComp
$Comp
L Comparator:LM339 U1
U 5 1 5D960E08
P 1400 6450
F 0 "U1" H 1358 6496 50  0000 L CNN
F 1 "LM339" H 1358 6405 50  0000 L CNN
F 2 "" H 1350 6550 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm339.pdf" H 1450 6650 50  0001 C CNN
	5    1400 6450
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR030
U 1 1 5D967003
P 4150 1100
F 0 "#PWR030" H 4150 950 50  0001 C CNN
F 1 "VCC" H 4167 1273 50  0000 C CNN
F 2 "" H 4150 1100 50  0001 C CNN
F 3 "" H 4150 1100 50  0001 C CNN
	1    4150 1100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR011
U 1 1 5D96B19B
P 2200 3850
F 0 "#PWR011" H 2200 3600 50  0001 C CNN
F 1 "GND" H 2205 3677 50  0000 C CNN
F 2 "" H 2200 3850 50  0001 C CNN
F 3 "" H 2200 3850 50  0001 C CNN
	1    2200 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 3750 2200 3800
Wire Wire Line
	2200 3800 2300 3800
Wire Wire Line
	2300 3800 2300 3750
Connection ~ 2200 3800
Wire Wire Line
	2200 3800 2200 3850
$Comp
L power:VCC #PWR016
U 1 1 5D96C3DF
P 2400 2300
F 0 "#PWR016" H 2400 2150 50  0001 C CNN
F 1 "VCC" H 2417 2473 50  0000 C CNN
F 2 "" H 2400 2300 50  0001 C CNN
F 3 "" H 2400 2300 50  0001 C CNN
	1    2400 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5D96DA31
P 1550 2950
F 0 "R1" V 1343 2950 50  0000 C CNN
F 1 "1k" V 1434 2950 50  0000 C CNN
F 2 "" V 1480 2950 50  0001 C CNN
F 3 "~" H 1550 2950 50  0001 C CNN
	1    1550 2950
	0    1    1    0   
$EndComp
$Comp
L Device:Q_NPN_BCE Q1
U 1 1 5D96E877
P 9100 2250
F 0 "Q1" H 9291 2296 50  0000 L CNN
F 1 "Q_NPN_BCE" H 9291 2205 50  0000 L CNN
F 2 "" H 9300 2350 50  0001 C CNN
F 3 "~" H 9100 2250 50  0001 C CNN
	1    9100 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV1
U 1 1 5D96F60F
P 8800 2050
F 0 "RV1" H 8730 2004 50  0000 R CNN
F 1 "10k" H 8730 2095 50  0000 R CNN
F 2 "" H 8800 2050 50  0001 C CNN
F 3 "~" H 8800 2050 50  0001 C CNN
	1    8800 2050
	-1   0    0    1   
$EndComp
$Comp
L Device:R R13
U 1 1 5D97013E
P 8800 2450
F 0 "R13" H 8870 2496 50  0000 L CNN
F 1 "1k" H 8870 2405 50  0000 L CNN
F 2 "" V 8730 2450 50  0001 C CNN
F 3 "~" H 8800 2450 50  0001 C CNN
	1    8800 2450
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR033
U 1 1 5D970A6C
P 8800 1450
F 0 "#PWR033" H 8800 1300 50  0001 C CNN
F 1 "VCC" H 8817 1623 50  0000 C CNN
F 2 "" H 8800 1450 50  0001 C CNN
F 3 "" H 8800 1450 50  0001 C CNN
	1    8800 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 2300 8800 2250
Wire Wire Line
	8800 2250 8600 2250
Wire Wire Line
	8600 2250 8600 2050
Wire Wire Line
	8600 2050 8650 2050
Wire Wire Line
	8900 2250 8800 2250
Connection ~ 8800 2250
$Comp
L power:GND #PWR034
U 1 1 5D972257
P 8800 2850
F 0 "#PWR034" H 8800 2600 50  0001 C CNN
F 1 "GND" H 8805 2677 50  0000 C CNN
F 2 "" H 8800 2850 50  0001 C CNN
F 3 "" H 8800 2850 50  0001 C CNN
	1    8800 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 2050 9200 1850
Wire Wire Line
	9200 1850 8800 1850
Wire Wire Line
	8800 1850 8800 1900
$Comp
L Device:R R12
U 1 1 5D9751D1
P 8800 1650
F 0 "R12" H 8870 1696 50  0000 L CNN
F 1 "1k" H 8870 1605 50  0000 L CNN
F 2 "" V 8730 1650 50  0001 C CNN
F 3 "~" H 8800 1650 50  0001 C CNN
	1    8800 1650
	1    0    0    -1  
$EndComp
Connection ~ 8800 1850
Wire Wire Line
	8800 1850 8800 1800
Wire Wire Line
	8800 1500 8800 1450
$Comp
L Device:R R14
U 1 1 5D97AD87
P 9200 2600
F 0 "R14" H 9270 2646 50  0000 L CNN
F 1 "1k" H 9270 2555 50  0000 L CNN
F 2 "" V 9130 2600 50  0001 C CNN
F 3 "~" H 9200 2600 50  0001 C CNN
	1    9200 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 2600 8800 2800
Wire Wire Line
	9200 2750 9200 2800
Wire Wire Line
	9200 2800 8800 2800
Connection ~ 8800 2800
Wire Wire Line
	8800 2800 8800 2850
Text GLabel 9200 1850 2    50   Input ~ 0
V_REF
Text GLabel 5800 2300 0    50   Input ~ 0
VREF
Text GLabel 5800 2750 0    50   Input ~ 0
VREF
Text GLabel 5800 3200 0    50   Input ~ 0
VREF
Text GLabel 5800 3650 0    50   Input ~ 0
VREF
Text GLabel 5800 4100 0    50   Input ~ 0
VREF
Text GLabel 5800 4550 0    50   Input ~ 0
VREF
Text GLabel 5800 5000 0    50   Input ~ 0
VREF
Text GLabel 5800 5450 0    50   Input ~ 0
VREF
Text GLabel 3450 2850 0    50   Input ~ 0
ADDR0
Text GLabel 3450 2950 0    50   Input ~ 0
ADDR1
Text GLabel 3450 3050 0    50   Input ~ 0
ADDR2
Text GLabel 10200 5600 2    50   Input ~ 0
ADDR0
Text GLabel 10200 5700 2    50   Input ~ 0
ADDR1
Text GLabel 10200 5800 2    50   Input ~ 0
ADDR2
$Comp
L Connector:Screw_Terminal_01x16 J5
U 1 1 5D9C019B
P 10200 3650
F 0 "J5" H 10280 3642 50  0000 L CNN
F 1 "Screw_Terminal_01x16" H 10280 3551 50  0000 L CNN
F 2 "" H 10200 3650 50  0001 C CNN
F 3 "~" H 10200 3650 50  0001 C CNN
	1    10200 3650
	1    0    0    -1  
$EndComp
Text GLabel 2800 3550 2    50   Input ~ 0
M7
Text GLabel 2800 3450 2    50   Input ~ 0
M6
Text GLabel 2800 3350 2    50   Input ~ 0
M5
Text GLabel 2800 3250 2    50   Input ~ 0
M4
Text GLabel 2800 3150 2    50   Input ~ 0
M3
Text GLabel 2800 3050 2    50   Input ~ 0
M2
Text GLabel 2800 2950 2    50   Input ~ 0
M1
Text GLabel 2800 2850 2    50   Input ~ 0
M0
Text GLabel 5800 2100 0    50   Input ~ 0
M0
Text GLabel 5800 2550 0    50   Input ~ 0
M1
Text GLabel 5800 3000 0    50   Input ~ 0
M2
Text GLabel 5800 3450 0    50   Input ~ 0
M3
Text GLabel 5800 3900 0    50   Input ~ 0
M4
Text GLabel 5800 4350 0    50   Input ~ 0
M5
Text GLabel 5800 4800 0    50   Input ~ 0
M6
Text GLabel 5800 5250 0    50   Input ~ 0
M7
$Comp
L power:VCC #PWR027
U 1 1 5D9E8301
P 3400 2300
F 0 "#PWR027" H 3400 2150 50  0001 C CNN
F 1 "VCC" H 3417 2473 50  0000 C CNN
F 2 "" H 3400 2300 50  0001 C CNN
F 3 "" H 3400 2300 50  0001 C CNN
	1    3400 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 2350 3400 2350
Wire Wire Line
	3400 2350 3400 2300
$Comp
L Analog_Switch:CD4051B U3
U 1 1 5D9EA455
P 2300 3050
F 0 "U3" H 2500 3450 50  0000 C CNN
F 1 "CD4051B" H 2500 3350 50  0000 C CNN
F 2 "" H 2450 2300 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd4052b.pdf" H 2280 3150 50  0001 C CNN
	1    2300 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 3800 1750 3050
Wire Wire Line
	1750 3050 1800 3050
Wire Wire Line
	1750 3800 2200 3800
Wire Wire Line
	1700 2950 1800 2950
Wire Wire Line
	1400 2950 1350 2950
Wire Wire Line
	1300 2550 1800 2550
Wire Wire Line
	1250 2650 1800 2650
Wire Wire Line
	1800 2750 1200 2750
Wire Wire Line
	2400 2350 2400 2300
Text GLabel 4850 1450 2    50   Input ~ 0
OUT0
Text GLabel 4850 1550 2    50   Input ~ 0
OUT1
Text GLabel 4850 1650 2    50   Input ~ 0
OUT2
Text GLabel 4850 1750 2    50   Input ~ 0
OUT3
Text GLabel 4850 1850 2    50   Input ~ 0
OUT4
Text GLabel 4850 1950 2    50   Input ~ 0
OUT5
Text GLabel 4850 2050 2    50   Input ~ 0
OUT6
Text GLabel 4850 2150 2    50   Input ~ 0
OUT7
$Comp
L power:VCC #PWR032
U 1 1 5DA06EAD
P 6500 1600
F 0 "#PWR032" H 6500 1450 50  0001 C CNN
F 1 "VCC" H 6517 1773 50  0000 C CNN
F 2 "" H 6500 1600 50  0001 C CNN
F 3 "" H 6500 1600 50  0001 C CNN
	1    6500 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 2200 6500 2200
Text GLabel 6500 2200 2    50   Input ~ 0
OUT0
Text GLabel 6600 2650 2    50   Input ~ 0
OUT1
Text GLabel 6700 3100 2    50   Input ~ 0
OUT2
Text GLabel 6800 3550 2    50   Input ~ 0
OUT3
Text GLabel 6900 4000 2    50   Input ~ 0
OUT4
Text GLabel 7000 4450 2    50   Input ~ 0
OUT5
Text GLabel 7100 4900 2    50   Input ~ 0
OUT6
Text GLabel 7200 5350 2    50   Input ~ 0
OUT7
$Comp
L power:GND #PWR037
U 1 1 5DA2AACA
P 9900 4500
F 0 "#PWR037" H 9900 4250 50  0001 C CNN
F 1 "GND" H 9905 4327 50  0000 C CNN
F 2 "" H 9900 4500 50  0001 C CNN
F 3 "" H 9900 4500 50  0001 C CNN
	1    9900 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 4450 9900 4450
Wire Wire Line
	9900 4450 9900 4500
Wire Wire Line
	9900 4450 9900 4250
Wire Wire Line
	9900 4250 10000 4250
Connection ~ 9900 4450
Wire Wire Line
	9900 4250 9900 4050
Wire Wire Line
	9900 4050 10000 4050
Connection ~ 9900 4250
Wire Wire Line
	9900 4050 9900 3850
Wire Wire Line
	9900 3850 10000 3850
Connection ~ 9900 4050
Wire Wire Line
	9900 3850 9900 3650
Wire Wire Line
	9900 3650 10000 3650
Connection ~ 9900 3850
Wire Wire Line
	9900 3650 9900 3450
Wire Wire Line
	9900 3450 10000 3450
Connection ~ 9900 3650
Wire Wire Line
	9900 3450 9900 3250
Wire Wire Line
	9900 3250 10000 3250
Connection ~ 9900 3450
Wire Wire Line
	9900 3250 9900 3050
Wire Wire Line
	9900 3050 10000 3050
Connection ~ 9900 3250
Text GLabel 10000 2950 0    50   Input ~ 0
M0
Text GLabel 10000 3150 0    50   Input ~ 0
M1
Text GLabel 10000 3350 0    50   Input ~ 0
M2
Text GLabel 10000 3550 0    50   Input ~ 0
M3
Text GLabel 10000 3750 0    50   Input ~ 0
M4
Text GLabel 10000 3950 0    50   Input ~ 0
M5
Text GLabel 10000 4150 0    50   Input ~ 0
M6
Text GLabel 10000 4350 0    50   Input ~ 0
M7
$Comp
L power:VCC #PWR05
U 1 1 5DA4597F
P 1900 6000
F 0 "#PWR05" H 1900 5850 50  0001 C CNN
F 1 "VCC" H 1917 6173 50  0000 C CNN
F 2 "" H 1900 6000 50  0001 C CNN
F 3 "" H 1900 6000 50  0001 C CNN
	1    1900 6000
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR012
U 1 1 5DA45C62
P 2200 6000
F 0 "#PWR012" H 2200 5850 50  0001 C CNN
F 1 "VCC" H 2217 6173 50  0000 C CNN
F 2 "" H 2200 6000 50  0001 C CNN
F 3 "" H 2200 6000 50  0001 C CNN
	1    2200 6000
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR017
U 1 1 5DA45E8C
P 2500 6000
F 0 "#PWR017" H 2500 5850 50  0001 C CNN
F 1 "VCC" H 2517 6173 50  0000 C CNN
F 2 "" H 2500 6000 50  0001 C CNN
F 3 "" H 2500 6000 50  0001 C CNN
	1    2500 6000
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR023
U 1 1 5DA46082
P 2800 6000
F 0 "#PWR023" H 2800 5850 50  0001 C CNN
F 1 "VCC" H 2817 6173 50  0000 C CNN
F 2 "" H 2800 6000 50  0001 C CNN
F 3 "" H 2800 6000 50  0001 C CNN
	1    2800 6000
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR07
U 1 1 5DA4631E
P 1900 6750
F 0 "#PWR07" H 1900 6600 50  0001 C CNN
F 1 "VCC" H 1917 6923 50  0000 C CNN
F 2 "" H 1900 6750 50  0001 C CNN
F 3 "" H 1900 6750 50  0001 C CNN
	1    1900 6750
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR014
U 1 1 5DA46550
P 2200 6750
F 0 "#PWR014" H 2200 6600 50  0001 C CNN
F 1 "VCC" H 2217 6923 50  0000 C CNN
F 2 "" H 2200 6750 50  0001 C CNN
F 3 "" H 2200 6750 50  0001 C CNN
	1    2200 6750
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR019
U 1 1 5DA46791
P 2500 6750
F 0 "#PWR019" H 2500 6600 50  0001 C CNN
F 1 "VCC" H 2517 6923 50  0000 C CNN
F 2 "" H 2500 6750 50  0001 C CNN
F 3 "" H 2500 6750 50  0001 C CNN
	1    2500 6750
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR01
U 1 1 5DA469A3
P 1300 6100
F 0 "#PWR01" H 1300 5950 50  0001 C CNN
F 1 "VCC" H 1317 6273 50  0000 C CNN
F 2 "" H 1300 6100 50  0001 C CNN
F 3 "" H 1300 6100 50  0001 C CNN
	1    1300 6100
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR03
U 1 1 5DA47110
P 1550 6100
F 0 "#PWR03" H 1550 5950 50  0001 C CNN
F 1 "VCC" H 1567 6273 50  0000 C CNN
F 2 "" H 1550 6100 50  0001 C CNN
F 3 "" H 1550 6100 50  0001 C CNN
	1    1550 6100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5DA47BC5
P 1300 6800
F 0 "#PWR02" H 1300 6550 50  0001 C CNN
F 1 "GND" H 1305 6627 50  0000 C CNN
F 2 "" H 1300 6800 50  0001 C CNN
F 3 "" H 1300 6800 50  0001 C CNN
	1    1300 6800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5DA48177
P 1550 6800
F 0 "#PWR04" H 1550 6550 50  0001 C CNN
F 1 "GND" H 1555 6627 50  0000 C CNN
F 2 "" H 1550 6800 50  0001 C CNN
F 3 "" H 1550 6800 50  0001 C CNN
	1    1550 6800
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C1
U 1 1 5DA49CA5
P 1900 6150
F 0 "C1" H 1992 6196 50  0000 L CNN
F 1 "100n" H 1992 6105 50  0000 L CNN
F 2 "" H 1900 6150 50  0001 C CNN
F 3 "~" H 1900 6150 50  0001 C CNN
	1    1900 6150
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C3
U 1 1 5DA4F529
P 2200 6150
F 0 "C3" H 2292 6196 50  0000 L CNN
F 1 "100n" H 2292 6105 50  0000 L CNN
F 2 "" H 2200 6150 50  0001 C CNN
F 3 "~" H 2200 6150 50  0001 C CNN
	1    2200 6150
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C5
U 1 1 5DA5320A
P 2500 6150
F 0 "C5" H 2592 6196 50  0000 L CNN
F 1 "100n" H 2592 6105 50  0000 L CNN
F 2 "" H 2500 6150 50  0001 C CNN
F 3 "~" H 2500 6150 50  0001 C CNN
	1    2500 6150
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C7
U 1 1 5DA5754A
P 2800 6150
F 0 "C7" H 2892 6196 50  0000 L CNN
F 1 "100n" H 2892 6105 50  0000 L CNN
F 2 "" H 2800 6150 50  0001 C CNN
F 3 "~" H 2800 6150 50  0001 C CNN
	1    2800 6150
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C2
U 1 1 5DA5B781
P 1900 6900
F 0 "C2" H 1992 6946 50  0000 L CNN
F 1 "100n" H 1992 6855 50  0000 L CNN
F 2 "" H 1900 6900 50  0001 C CNN
F 3 "~" H 1900 6900 50  0001 C CNN
	1    1900 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C4
U 1 1 5DA5F29D
P 2200 6900
F 0 "C4" H 2292 6946 50  0000 L CNN
F 1 "100n" H 2292 6855 50  0000 L CNN
F 2 "" H 2200 6900 50  0001 C CNN
F 3 "~" H 2200 6900 50  0001 C CNN
	1    2200 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C6
U 1 1 5DA5FD6B
P 2500 6900
F 0 "C6" H 2592 6946 50  0000 L CNN
F 1 "100n" H 2592 6855 50  0000 L CNN
F 2 "" H 2500 6900 50  0001 C CNN
F 3 "~" H 2500 6900 50  0001 C CNN
	1    2500 6900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5DA60612
P 1900 6300
F 0 "#PWR06" H 1900 6050 50  0001 C CNN
F 1 "GND" H 1905 6127 50  0000 C CNN
F 2 "" H 1900 6300 50  0001 C CNN
F 3 "" H 1900 6300 50  0001 C CNN
	1    1900 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5DA60BA8
P 2200 6300
F 0 "#PWR013" H 2200 6050 50  0001 C CNN
F 1 "GND" H 2205 6127 50  0000 C CNN
F 2 "" H 2200 6300 50  0001 C CNN
F 3 "" H 2200 6300 50  0001 C CNN
	1    2200 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR018
U 1 1 5DA60DCE
P 2500 6300
F 0 "#PWR018" H 2500 6050 50  0001 C CNN
F 1 "GND" H 2505 6127 50  0000 C CNN
F 2 "" H 2500 6300 50  0001 C CNN
F 3 "" H 2500 6300 50  0001 C CNN
	1    2500 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR024
U 1 1 5DA61024
P 2800 6300
F 0 "#PWR024" H 2800 6050 50  0001 C CNN
F 1 "GND" H 2805 6127 50  0000 C CNN
F 2 "" H 2800 6300 50  0001 C CNN
F 3 "" H 2800 6300 50  0001 C CNN
	1    2800 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5DA611CC
P 1900 7050
F 0 "#PWR08" H 1900 6800 50  0001 C CNN
F 1 "GND" H 1905 6877 50  0000 C CNN
F 2 "" H 1900 7050 50  0001 C CNN
F 3 "" H 1900 7050 50  0001 C CNN
	1    1900 7050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR015
U 1 1 5DA615D8
P 2200 7050
F 0 "#PWR015" H 2200 6800 50  0001 C CNN
F 1 "GND" H 2205 6877 50  0000 C CNN
F 2 "" H 2200 7050 50  0001 C CNN
F 3 "" H 2200 7050 50  0001 C CNN
	1    2200 7050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR020
U 1 1 5DA61ACF
P 2500 7050
F 0 "#PWR020" H 2500 6800 50  0001 C CNN
F 1 "GND" H 2505 6877 50  0000 C CNN
F 2 "" H 2500 7050 50  0001 C CNN
F 3 "" H 2500 7050 50  0001 C CNN
	1    2500 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 6050 1900 6000
Wire Wire Line
	2200 6050 2200 6000
Wire Wire Line
	2500 6050 2500 6000
Wire Wire Line
	2800 6050 2800 6000
Wire Wire Line
	2500 6800 2500 6750
Wire Wire Line
	2200 6800 2200 6750
Wire Wire Line
	1900 6800 1900 6750
Wire Wire Line
	1900 7050 1900 7000
Wire Wire Line
	2200 7050 2200 7000
Wire Wire Line
	2500 7050 2500 7000
$Comp
L Device:C_Small C8
U 1 1 5DA89973
P 2850 6900
F 0 "C8" H 2942 6946 50  0000 L CNN
F 1 "100n" H 2942 6855 50  0000 L CNN
F 2 "" H 2850 6900 50  0001 C CNN
F 3 "~" H 2850 6900 50  0001 C CNN
	1    2850 6900
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR025
U 1 1 5DA89B04
P 2850 6750
F 0 "#PWR025" H 2850 6600 50  0001 C CNN
F 1 "VCC" H 2867 6923 50  0000 C CNN
F 2 "" H 2850 6750 50  0001 C CNN
F 3 "" H 2850 6750 50  0001 C CNN
	1    2850 6750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR026
U 1 1 5DA89F0B
P 2850 7050
F 0 "#PWR026" H 2850 6800 50  0001 C CNN
F 1 "GND" H 2855 6877 50  0000 C CNN
F 2 "" H 2850 7050 50  0001 C CNN
F 3 "" H 2850 7050 50  0001 C CNN
	1    2850 7050
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR028
U 1 1 5DA8A1CA
P 3100 6000
F 0 "#PWR028" H 3100 5850 50  0001 C CNN
F 1 "VCC" H 3117 6173 50  0000 C CNN
F 2 "" H 3100 6000 50  0001 C CNN
F 3 "" H 3100 6000 50  0001 C CNN
	1    3100 6000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR029
U 1 1 5DA8A46B
P 3100 6300
F 0 "#PWR029" H 3100 6050 50  0001 C CNN
F 1 "GND" H 3105 6127 50  0000 C CNN
F 2 "" H 3100 6300 50  0001 C CNN
F 3 "" H 3100 6300 50  0001 C CNN
	1    3100 6300
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C9
U 1 1 5DA8A60D
P 3100 6150
F 0 "C9" H 3192 6196 50  0000 L CNN
F 1 "100n" H 3192 6105 50  0000 L CNN
F 2 "" H 3100 6150 50  0001 C CNN
F 3 "~" H 3100 6150 50  0001 C CNN
	1    3100 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 6050 3100 6000
Wire Wire Line
	2850 7050 2850 7000
Wire Wire Line
	2850 6800 2850 6750
Wire Wire Line
	3100 6300 3100 6250
Wire Wire Line
	2800 6300 2800 6250
Wire Wire Line
	2500 6300 2500 6250
Wire Wire Line
	2200 6300 2200 6250
Wire Wire Line
	1900 6300 1900 6250
Wire Wire Line
	1300 6150 1300 6100
Wire Wire Line
	1550 6150 1550 6100
Wire Wire Line
	1550 6800 1550 6750
Wire Wire Line
	1300 6800 1300 6750
NoConn ~ 8800 2200
Wire Wire Line
	1200 1450 1500 1450
Wire Wire Line
	1200 1450 1200 2750
Wire Wire Line
	1250 1550 1500 1550
Wire Wire Line
	1250 1550 1250 2650
Wire Wire Line
	1300 1650 1500 1650
Wire Wire Line
	1300 1650 1300 2550
Wire Wire Line
	1350 1750 1500 1750
Wire Wire Line
	1350 1750 1350 2950
Wire Wire Line
	1500 1850 1400 1850
Wire Wire Line
	1400 1850 1400 2250
Wire Wire Line
	1400 2250 2250 2250
Wire Wire Line
	2250 2250 2250 1900
Wire Wire Line
	2250 1900 2100 1900
Connection ~ 2100 1900
Wire Wire Line
	2100 1900 2100 1850
Wire Wire Line
	1500 1950 1450 1950
Wire Wire Line
	1450 1950 1450 2200
Wire Wire Line
	1450 2200 2200 2200
Wire Wire Line
	2200 2200 2200 1950
Connection ~ 2200 1950
$Comp
L Interface_Expansion:MCP23S17_SO U4
U 1 1 5D985327
P 4150 2250
F 0 "U4" H 4150 2600 50  0000 C CNN
F 1 "MCP23S17_SO" H 4150 2300 50  0000 C CNN
F 2 "Package_SO:SOIC-28W_7.5x17.9mm_P1.27mm" H 4350 1250 50  0001 L CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20001952C.pdf" H 4350 1150 50  0001 L CNN
	1    4150 2250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR031
U 1 1 5D9666D2
P 4150 3400
F 0 "#PWR031" H 4150 3150 50  0001 C CNN
F 1 "GND" H 4155 3227 50  0000 C CNN
F 2 "" H 4150 3400 50  0001 C CNN
F 3 "" H 4150 3400 50  0001 C CNN
	1    4150 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 3400 4150 3350
NoConn ~ 4850 2350
NoConn ~ 4850 2450
NoConn ~ 4850 2550
NoConn ~ 4850 2650
NoConn ~ 4850 2750
NoConn ~ 4850 2850
NoConn ~ 4850 2950
NoConn ~ 4850 3050
Wire Wire Line
	4150 1100 4150 1150
Wire Wire Line
	2000 1650 3450 1650
Wire Wire Line
	2000 1750 3450 1750
Wire Wire Line
	2000 1450 3450 1450
$Comp
L Device:R R2
U 1 1 5DA198C5
P 3200 1550
F 0 "R2" V 3407 1550 50  0000 C CNN
F 1 "100" V 3316 1550 50  0000 C CNN
F 2 "" V 3130 1550 50  0001 C CNN
F 3 "~" H 3200 1550 50  0001 C CNN
	1    3200 1550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3450 1550 3350 1550
Wire Wire Line
	3050 1550 2000 1550
$Comp
L Connector:Conn_01x02_Female J6
U 1 1 5DA280AA
P 3050 2050
F 0 "J6" H 2942 2235 50  0000 C CNN
F 1 "INT_OUT" H 2942 2144 50  0000 C CNN
F 2 "" H 3050 2050 50  0001 C CNN
F 3 "~" H 3050 2050 50  0001 C CNN
	1    3050 2050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3250 2050 3450 2050
Wire Wire Line
	3450 2150 3250 2150
$Comp
L power:VDD #PWR010
U 1 1 5DA3E849
P 2200 1850
F 0 "#PWR010" H 2200 1700 50  0001 C CNN
F 1 "VDD" H 2217 2023 50  0000 C CNN
F 2 "" H 2200 1850 50  0001 C CNN
F 3 "" H 2200 1850 50  0001 C CNN
	1    2200 1850
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR021
U 1 1 5DA3FA3E
P 3650 6000
F 0 "#PWR021" H 3650 5850 50  0001 C CNN
F 1 "VCC" H 3667 6173 50  0000 C CNN
F 2 "" H 3650 6000 50  0001 C CNN
F 3 "" H 3650 6000 50  0001 C CNN
	1    3650 6000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J7
U 1 1 5DA40C83
P 3650 6300
F 0 "J7" V 3496 6348 50  0000 L CNN
F 1 "POWER_5V" V 3587 6348 50  0000 L CNN
F 2 "" H 3650 6300 50  0001 C CNN
F 3 "~" H 3650 6300 50  0001 C CNN
	1    3650 6300
	0    1    1    0   
$EndComp
Wire Wire Line
	3650 6100 3650 6000
$Comp
L Device:R_Network08 RN1
U 1 1 5DAC039E
P 6900 1850
F 0 "RN1" H 7288 1896 50  0000 L CNN
F 1 "R_Network08" H 7288 1805 50  0000 L CNN
F 2 "Resistor_THT:R_Array_SIP9" V 7375 1850 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 6900 1850 50  0001 C CNN
	1    6900 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 1650 6500 1600
Wire Wire Line
	6500 2050 6500 2200
Wire Wire Line
	6400 2650 6600 2650
Wire Wire Line
	6600 2650 6600 2050
Wire Wire Line
	6700 3100 6700 2050
Wire Wire Line
	6400 3100 6700 3100
Wire Wire Line
	6400 3550 6800 3550
Wire Wire Line
	6800 3550 6800 2050
Wire Wire Line
	6400 4000 6900 4000
Wire Wire Line
	6900 4000 6900 2050
Wire Wire Line
	6400 4450 7000 4450
Wire Wire Line
	7000 4450 7000 2050
Wire Wire Line
	6400 4900 7100 4900
Wire Wire Line
	7100 4900 7100 2050
Wire Wire Line
	6400 5350 7200 5350
Wire Wire Line
	7200 5350 7200 2050
$Comp
L Connector:Conn_01x03_Male J2
U 1 1 5DB098C5
P 9100 5250
F 0 "J2" H 9208 5531 50  0000 C CNN
F 1 "Conn_01x03_Male" H 9208 5440 50  0000 C CNN
F 2 "" H 9100 5250 50  0001 C CNN
F 3 "~" H 9100 5250 50  0001 C CNN
	1    9100 5250
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J3
U 1 1 5DB09E63
P 9100 5700
F 0 "J3" H 9208 5981 50  0000 C CNN
F 1 "Conn_01x03_Male" H 9208 5890 50  0000 C CNN
F 2 "" H 9100 5700 50  0001 C CNN
F 3 "~" H 9100 5700 50  0001 C CNN
	1    9100 5700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J4
U 1 1 5DB0A55E
P 9100 6100
F 0 "J4" H 9208 6381 50  0000 C CNN
F 1 "Conn_01x03_Male" H 9208 6290 50  0000 C CNN
F 2 "" H 9100 6100 50  0001 C CNN
F 3 "~" H 9100 6100 50  0001 C CNN
	1    9100 6100
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Network03 RN2
U 1 1 5DB0B9E2
P 9600 4800
F 0 "RN2" H 9788 4846 50  0000 L CNN
F 1 "4.7k" H 9788 4755 50  0000 L CNN
F 2 "Resistor_THT:R_Array_SIP4" V 9875 4800 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 9600 4800 50  0001 C CNN
	1    9600 4800
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR035
U 1 1 5DB0CD8D
P 9500 4500
F 0 "#PWR035" H 9500 4350 50  0001 C CNN
F 1 "VCC" H 9517 4673 50  0000 C CNN
F 2 "" H 9500 4500 50  0001 C CNN
F 3 "" H 9500 4500 50  0001 C CNN
	1    9500 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9500 4500 9500 4600
$Comp
L power:GND #PWR022
U 1 1 5DB1223C
P 9450 6250
F 0 "#PWR022" H 9450 6000 50  0001 C CNN
F 1 "GND" H 9455 6077 50  0000 C CNN
F 2 "" H 9450 6250 50  0001 C CNN
F 3 "" H 9450 6250 50  0001 C CNN
	1    9450 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 5250 10100 5250
Wire Wire Line
	10100 5600 10200 5600
Wire Wire Line
	10100 5250 10100 5600
Wire Wire Line
	10200 5700 9300 5700
Wire Wire Line
	9300 6100 10100 6100
Wire Wire Line
	10100 6100 10100 5800
Wire Wire Line
	10100 5800 10200 5800
Wire Wire Line
	9300 5150 9500 5150
Wire Wire Line
	9500 5150 9500 5000
Wire Wire Line
	9300 5600 9600 5600
Wire Wire Line
	9600 5600 9600 5000
Wire Wire Line
	9300 6000 9700 6000
Wire Wire Line
	9700 6000 9700 5000
Wire Wire Line
	9300 5350 9450 5350
Wire Wire Line
	9450 5350 9450 5800
Wire Wire Line
	9300 5800 9450 5800
Connection ~ 9450 5800
Wire Wire Line
	9450 5800 9450 6200
Wire Wire Line
	9300 6200 9450 6200
Connection ~ 9450 6200
Wire Wire Line
	9450 6200 9450 6250
$EndSCHEMATC
